<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

//Get DB instance. function is defined in config.php
$db = getDbInstance();

//Get Dashboard information
$numMiliter = $db->getValue("militer", "count(*)");
$numPns 		= $db->getValue("asn", "count(*)");
$numTks 		= $db->getValue("tks", "count(*)");
$numUsers 	= $db->getValue("users", "count(*)");

include_once('includes/header.php');
include_once('includes/sidebar.php');
?>


<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>

	</div>

	<!-- Content Row -->
	<div class="row">

		<!-- Earnings (Monthly) Card Example -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-left-primary shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 font-weight-bold text-primary text-uppercase mb-1">Militer</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $numMiliter ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Earnings (Monthly) Card Example -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-left-success shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 font-weight-bold text-success text-uppercase mb-1">PNS</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $numPns ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Earnings (Monthly) Card Example -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-left-info shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 font-weight-bold text-info text-uppercase mb-1">TKS</div>
							<div class="row no-gutters align-items-center">
								<div class="col-auto">
									<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?= $numTks ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Pending Requests Card Example -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 font-weight-bold text-warning text-uppercase mb-1">Admin</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $numUsers ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
<!-- /.container-fluid -->

<!-- /#page-wrapper -->

<?php include_once('includes/footer.php'); ?>