<?php
session_start();
require_once '../../config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

//Only super admin is allowed to access this page
if ($_SESSION['admin_type'] !== 'super') {
    // show permission denied message
    echo 'Permission Denied';
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $data_to_store = filter_input_array(INPUT_POST);
    $db = getDbInstance();
    //Check whether the user name already exists ; 
    $db->where('user_name', $data_to_store['user_name']);
    $db->get('users');

    if ($db->count >= 1) {
        $_SESSION['failure'] = "User name already exists";
        header('location: add_users.php');
        exit();
    }

    //Encrypt password
    $data_to_store['password'] = password_hash($data_to_store['password'], PASSWORD_DEFAULT);
    //reset db instance
    $db = getDbInstance();
    $last_id = $db->insert('users', $data_to_store);
    if ($last_id) {
        $_SESSION['success'] = "Pengguna berhasil ditambahkan";
        header('location: users.php');
    } else {
        $_SESSION['failure'] = "Pengguna gagal ditambahkan";
        header('location: users.php');
    }
    exit();
}
