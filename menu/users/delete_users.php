<?php
session_start();
require_once '../../config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

$del_id = filter_input(INPUT_POST, 'del_id');
$db = getDbInstance();

if ($_SESSION['admin_type'] != 'super') {
    header('HTTP/1.1 401 Unauthorized', true, 401);
    exit("401 Unauthorized");
}

if ($del_id && $_SERVER['REQUEST_METHOD'] == 'POST') {

    $count = $db->getValue("users", "count(*)");
    if ($count <= 1) {
        $_SESSION['info'] = "Pengguna Harus lebih dari 1!";
        header('location: users.php');
        exit;
    }

    $db->where('id', $del_id);
    $stat = $db->delete('users');
    if ($stat) {
        $_SESSION['info'] = "Pengguna Berhasil dihapus!";
        header('location: users.php');
        exit;
    }
}
