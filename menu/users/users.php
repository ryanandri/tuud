<?php
session_start();
require_once '../../config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';
require_once BASE_PATH . '/lib/Users/Users.php';

$users = new Users();

// Only super admin is allowed to access this page
if ($_SESSION['admin_type'] !== 'super') {
    // Show permission denied message
    header('HTTP/1.1 401 Unauthorized', true, 401);
    exit('401 Unauthorized');
}

// Get Input data from query string
$search_string = filter_input(INPUT_GET, 'search_string');
$filter_col = filter_input(INPUT_GET, 'filter_col');
$order_by = filter_input(INPUT_GET, 'order_by');
$del_id = filter_input(INPUT_GET, 'del_id');

// Per page limit for pagination.
$pagelimit = 20;

// Get current page.
$page = filter_input(INPUT_GET, 'page');
if (!$page) {
    $page = 1;
}

// If filter types are not selected we show latest added data first
if (!$filter_col) {
    $filter_col = 'id';
}
if (!$order_by) {
    $order_by = 'Asc';
}

//Get DB instance. i.e instance of MYSQLiDB Library
$db = getDbInstance();
$select = array('id', 'user_name', 'admin_type');

//Start building query according to input parameters.
// If search string
if ($search_string) {
    $db->where('user_name', '%' . $search_string . '%', 'like');
}

//If order by option selected
if ($order_by) {
    $db->orderBy($filter_col, $order_by);
}

// Set pagination limit
$db->pageLimit = $pagelimit;

// Get result of the query.
$rows = $db->arraybuilder()->paginate('users', $page, $select);
$total_pages = $db->totalPages;

include BASE_PATH . '/includes/header.php';
include BASE_PATH . '/includes/sidebar.php';
?>
<!-- Main container -->
<div id="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="page-header mb-4">Pengguna</h2>
            </div>
            <div class="col-lg-6">
                <div class="page-action-links text-right">
                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#tambah-pengguna"><i class=" fas fa-plus pr-3"></i>Tambah</a>
                </div>
            </div>
        </div>
        <?php include BASE_PATH . '/includes/flash_messages.php'; ?>

        <?php
        if (isset($del_stat) && $del_stat == 1) {
            echo '<div class="alert alert-info">Successfully deleted</div>';
        }
        ?>

        <!-- Filters -->
        <div class="well text-center filter-form">
            <form class="form form-inline" action="">
                <label for="input_search" class="mr-2">Search</label>
                <input type="text" class="form-control" id="input_search" name="search_string" value="<?php echo htmlspecialchars($search_string, ENT_QUOTES, 'UTF-8'); ?>">
                <label for="input_order" class="ml-2 mr-2">Order By</label>
                <select name="filter_col" class="form-control">
                    <?php
                    foreach ($users->setOrderingValues() as $opt_value => $opt_name) : ($order_by === $opt_value) ? $selected = 'selected' : $selected = '';
                        echo ' <option value="' . $opt_value . '" ' . $selected . '>' . $opt_name . '</option>';
                    endforeach;
                    ?>
                </select>
                <select name="order_by" class="form-control ml-2" id="input_order">
                    <option value="Asc" <?php if ($order_by == 'Asc') {
                                            echo 'selected';
                                        } ?>>Asc</option>
                    <option value="Desc" <?php
                                            if ($order_by == 'Desc') {
                                                echo 'selected';
                                            }
                                            ?>>Desc</option>
                </select>
                <input type="submit" value="Go" class="btn btn-primary ml-2">
            </form>
        </div>
        <hr>
        <!-- //Filters -->

        <!-- Table -->
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" width="5%">No</th>
                    <th width="40%">Name</th>
                    <th width="35%">Admin type</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($rows as $row) : ?>
                    <tr>
                        <td class="text-center"><?php echo $no++ ?></td>
                        <td><?php echo htmlspecialchars($row['user_name']); ?></td>
                        <td><?php echo htmlspecialchars($row['admin_type']); ?></td>
                        <td class="text-center">
                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirm-update-<?php echo $row['id']; ?>">
                                <i class="fas fa-file-alt fa-lg text-white" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirm-delete-<?php echo $row['id']; ?>">
                                <i class="fas fa-trash-alt fa-lg text-white" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    <!-- Update pengguna Modal -->
                    <div class="modal fade" id="confirm-update-<?php echo $row['id']; ?>" role="dialog">
                        <div class="modal-dialog">
                            <form action="edit_users.php?user_id=<?php echo $row['id']; ?>" method="POST">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Update Pengguna</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-sm-12 p-0">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <input class="form-control" name="user_name" type="text" id="user_name" placeholder="Masukan Username" required="" value="<?php echo $row['user_name']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 p-0">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <input class="form-control" name="password_lama" type="password" id="password_lama" placeholder="Masukan Password Lama" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 p-0">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <input class="form-control" name="password_baru" type="password" id="password_baru" placeholder="Masukan Password Baru" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="admin_type" id="admin_type">
                                                <option value="admin" <?php echo ($row['admin_type'] == 'admin') ? "selected" : ""; ?>>Administrator</option>
                                                <option value="super" <?php echo ($row['admin_type'] == 'super') ? "selected" : ""; ?>>Super Administrator</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary btn-lg form-control">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Delete pengguna Modal -->
                    <div class="modal fade" id="confirm-delete-<?php echo $row['id']; ?>" role="dialog">
                        <div class="modal-dialog">
                            <form action="delete_users.php" method="POST">
                                <!-- Modal content -->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Konfirmasi Hapus Pengguna</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="del_id" id="del_id" value="<?php echo $row['id']; ?>">
                                        <p>Hapus pengguna -> <b><?php echo $row['user_name']; ?></b></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Hapus</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- //Delete Confirmation Modal -->
                <?php endforeach; ?>
            </tbody>
        </table>
        <!-- //Table -->

        <!-- Modal Tambah Pengguna -->
        <div class="modal fade" id="tambah-pengguna" role="dialog">
            <div class="modal-dialog">
                <form action="add_users.php" method="POST">
                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah Pengguna</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-12 p-0">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input class="form-control" name="user_name" type="text" id="user_name" placeholder="Masukan Username" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 p-0">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input class="form-control" name="password" type="password" id="password" placeholder="Masukan Password" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <select class="form-control" name="admin_type" id="admin_type">
                                    <option value="admin">Administrator</option>
                                    <option value="super">Super Administrator</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btn-lg form-control">Simpan</button>
                        </div>
                    </div>
            </div>
        </div>

        <!-- Pagination -->
        <div class="text-center">
            <?php
            if (!empty($_GET)) {
                // We must unset $_GET[page] if previously built by http_build_query function
                unset($_GET['page']);
                // To keep the query sting parameters intact while navigating to next/prev page,
                $http_query = "?" . http_build_query($_GET);
            } else {
                $http_query = "?";
            }
            // Show pagination links
            if ($total_pages > 1) {
                echo '<ul class="pagination text-center">';
                for ($i = 1; $i <= $total_pages; $i++) {
                    ($page == $i) ? $li_class = ' class="active"' : $li_class = '';
                    echo '<li' . $li_class . '><a href="<?= BASE_URL ?>/menu/users/users.php' . $http_query . '&page=' . $i . '">' . $i . '</a></li>';
                }
                echo '</ul>';
            }
            ?>
        </div>
        <!-- //Pagination -->
    </div>
</div>
<!-- //Main container -->
<?php include BASE_PATH . '/includes/footer.php'; ?>