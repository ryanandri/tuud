<?php
session_start();
require_once '../../config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if ($_SESSION['admin_type'] !== 'super') {
		// show permission denied message
		echo 'Permission Denied';
		exit();
	}

	$user_id = filter_input(INPUT_GET, 'user_id', FILTER_VALIDATE_INT);
	$db = getDbInstance();
	$db->where('id', $user_id);
	$row = $db->get('users');

	if ($db->count >= 1) {
		$passhash = $row[0]['password'];
		$pass_lama = filter_input(INPUT_POST, 'password_lama');
		if (!password_verify($pass_lama, $passhash)) {
			$_SESSION['failure'] = "Password Salah!";
			header('location: users.php');
			exit();
		}
	}

	$data_to_update['user_name'] = filter_input(INPUT_POST, 'user_name');
	$data_to_update['admin_type'] = filter_input(INPUT_POST, 'admin_type');
	$pass_baru = filter_input(INPUT_POST, 'password_baru');
	if (!empty($pass_baru)) {
		$data_to_update['password'] =
			password_hash(filter_input(INPUT_POST, 'password_baru'), PASSWORD_DEFAULT);
	}

	$db = getDbInstance();
	$db->where('id', $user_id);
	$stat = $db->update('users', $data_to_update);

	if ($stat) {
		$_SESSION['success'] = "User berhasil diubah";
	} else {
		$_SESSION['failure'] = "Gagal mengubah user : " . $db->getLastError();
	}

	header('location: users.php');
	exit();
}
